export const SYNOPTIC_CONFIG = {
    stationRadius: 10,
    interStation: 60,
    stationsColor: '#3f51b5',
    stationsHighlight: 'orange',
    lineWidth: 6,
    lineColor: 'lightgray',
    connectionSize: 10,
    connectionBorder: 2,
    height: 150,
    transitionDuration: 500
};
