import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/internal/operators";

@Component({
    selector: 'app-line',
    templateUrl: './line.component.html',
    styleUrls: ['./line.component.less']
})
export class LineComponent implements OnInit {
    showMap: boolean;
    selectedId: number;
    private _closeChildOutlet: Subject<boolean> = new Subject();

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {

    }

    onActivate(childComponent) {
        childComponent.route.paramMap.pipe(
            takeUntil(this._closeChildOutlet)
        ).subscribe((params: ParamMap) => {
            this.selectedId = +params.get('id');
        });
        this.showMap = true;
    }

    onDeactivate(event) {
        this._closeChildOutlet.next(true);
        this.selectedId = null;
        this.showMap = false;
    }
}
