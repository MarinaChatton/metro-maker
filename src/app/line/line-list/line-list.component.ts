import {Component, OnInit, Input} from "@angular/core";
import {Observable} from "rxjs";
import {Line} from "../../shared/line/line";
import {Router, ActivatedRoute} from "@angular/router";
import {LineService} from "../../shared/line/line.service";

@Component({
    selector: 'app-line-list',
    templateUrl: './line-list.component.html',
    styleUrls: ['./line-list.component.less']
})
export class LineListComponent implements OnInit {
    lines$: Observable<Line[]>;
    @Input('selectedId') selectedId: number;

    constructor(private lineService: LineService, private router: Router, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.lines$ = this.lineService.getAll();
    }

    showDetails(line: Line) {
        this.router.navigate([line.id], {relativeTo: this.route});
    }
}
