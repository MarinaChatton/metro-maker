import {SYNOPTIC_CONFIG} from "./synoptic.config";
import {Station} from "../shared/station/station";
import * as d3 from "d3";
import {Line} from "../shared/line/line";

export type StationWithConnections = [Station, Line[]];
export type OnStationWithConnectionsMouseEvent = (stationWithConnections: StationWithConnections) => void;
export type OnLineMouseEvent = (line: Line) => void;

export class Synoptic {
    private _svg;
    private _connector;

    constructor(svg) {
        this._init(svg);
    }

    render(data: StationWithConnections[], onStationClick: OnStationWithConnectionsMouseEvent, onStationHover: OnStationWithConnectionsMouseEvent, onStationMouseOut: OnStationWithConnectionsMouseEvent, onConnectionClick: OnLineMouseEvent) {
        const stationsCount = (data || []).length;
        const currentWidth = this._svg.attr('width');
        const newWidth = this._getStationX(stationsCount - 1) + SYNOPTIC_CONFIG.height;

        // adapt svg width
        // increase immediately the width / decrease slowly to allow the children animations to play
        this._svg
            .transition().duration(newWidth > currentWidth ? 0 : 2 * SYNOPTIC_CONFIG.transitionDuration)
            .attr('width', newWidth)
            .attr('height', SYNOPTIC_CONFIG.height + this._getHeightBelow(data));

        // update connecting line
        if (stationsCount > 0) {
            this._connector
                .transition().duration(SYNOPTIC_CONFIG.transitionDuration)
                .attr('x2', this._getStationX(stationsCount - 1));
        }

        // stations rendering
        const stations = this._getCurrentStationGroups(data);
        const newStations = this._addStations(stations, onStationClick, onStationHover, onStationMouseOut);
        this._deleteStations(stations);

        // connections rendering
        const connections = this._getCurrentConnexions(stations.merge(newStations));
        this._addConnections(connections, onConnectionClick);
        this._deleteConnexions(connections);
    }

    highlightStation = (station: Station) => {
        const stations = this._svg.selectAll('g.station');
        const stationEl = document.getElementById('station-' + station.id);// todo: find a better way to get the node?
        this._resetStationsColor(stations);
        if (stationEl) {
            this._setStationColor(stationEl, true);
            //noinspection TypeScriptValidateTypes
            stationEl.scrollIntoView({behavior: "smooth"});// added noinspection to avoid an ide error for scrollIntoView args type
        }
    };

    private _init(svg) {
        this._svg = svg.attr('height', SYNOPTIC_CONFIG.height);
        this._connector = this._svg.append('line')
            .attr('x1', this._getStationX(0))
            .attr('y1', this._getStationY)
            .attr('x2', this._getStationX(0))
            .attr('y2', this._getStationY)
            .attr('stroke-width', SYNOPTIC_CONFIG.lineWidth)
            .attr('stroke', SYNOPTIC_CONFIG.lineColor);
    }

    // STATIONS
    private _getCurrentStationGroups = (data: StationWithConnections[]): any => {
        const currentStationGroups = this._svg.selectAll('g.station-group').data(data);
        this._setStationsId(currentStationGroups);
        const currentStations = currentStationGroups.select('g.station');
        this._updateStationCircles(currentStations);
        this._updateStationNames(currentStations);
        return currentStationGroups;

    };

    private _addStations = (currentStationGroups, onStationClick, onStationHover, onStationMouseOut): any => {
        const newStationGroups = currentStationGroups.enter().append('g')
            .classed('station-group', true)
            .attr('y', this._getStationY)
            .attr('transform', (stationWithConnections, i) => this._getStationTransform(i));
        this._setStationsId(newStationGroups);
        const newStations = newStationGroups.append('g').classed('station', true);
        this._setStationsListeners(newStations, onStationClick, onStationHover, onStationMouseOut);
        this._addStationCircles(newStations);
        this._addStationNames(newStations);
        return newStationGroups;
    };

    private _deleteStations = (currentStationGroups) => {
        const deletedStationGroups = currentStationGroups.exit();
        const deletedStations = deletedStationGroups.select('g.station');
        this._deleteStationCircles(deletedStations);
        this._deleteStationNames(deletedStations);
        deletedStationGroups
            .transition().duration(SYNOPTIC_CONFIG.transitionDuration)
            .remove();
    };

    private _setStationsId = (stationGroups) => {
        stationGroups.attr('id', ([station, lines]) => 'station-' + station.id);
    };

    private _setStationsListeners = (newStations, onStationClick, onStationHover, onStationMouseOut) => {
        const that = this;
        const stations = this._svg.selectAll('g.station');
        newStations
            .style("cursor", "pointer")
            .on('mouseover', function (wp) {
                that._resetStationsColor(stations);
                that._setStationColor(this, true);
                onStationHover(wp);
            })
            .on('mouseout', function (wp) {
                that._resetStationsColor(stations);
                onStationMouseOut(wp);
            })
            .on('click', function (wp) {
                onStationClick(wp);
            });
    };

    private _setStationColor = (station, highlight?) => {
        const stationGroup = d3.select(station);
        const circle = stationGroup.select('circle');
        const name = stationGroup.select('text');
        circle.attr('fill', highlight ? SYNOPTIC_CONFIG.stationsHighlight : SYNOPTIC_CONFIG.stationsColor);
        name.attr('fill', highlight ? SYNOPTIC_CONFIG.stationsHighlight : SYNOPTIC_CONFIG.stationsColor);
    };

    private _resetStationsColor = (stations) => {
        const that = this;
        stations.each(function () {
            that._setStationColor(this, false);
        });
    };


    // STATIONS CIRCLE
    private _updateStationCircles = (currentStations) => {
        const currentStationCircles = currentStations.select('circle');
        this._setStationCircles(currentStationCircles);
        currentStationCircles.attr("r", SYNOPTIC_CONFIG.stationRadius);
    };

    private _addStationCircles = (newStations) => {
        const newStationCircles = newStations.append('circle');
        this._setStationCircles(newStationCircles);
        newStationCircles
            .transition().duration(SYNOPTIC_CONFIG.transitionDuration)
            .attr("r", SYNOPTIC_CONFIG.stationRadius);
    };

    private _deleteStationCircles = (deletedStations) => {
        deletedStations.select('circle')
            .transition().duration(SYNOPTIC_CONFIG.transitionDuration)
            .attr("r", 0)
            .remove();
    };

    private _setStationCircles = (stationCircles) => {
        stationCircles
            .attr('cy', this._getStationY)
            .attr('r', 0)
            .attr('fill', SYNOPTIC_CONFIG.stationsColor);
    };

    // STATIONS NAME
    private _updateStationNames = (currentStations): any => {
        const currentStationNames = currentStations.select('text');
        this._setStationNames(currentStationNames);
        return currentStationNames.attr("opacity", 1);
    };

    private _addStationNames = (newStations) => {
        const newStationNames = newStations.append('text');
        this._setStationNames(newStationNames);
        newStationNames
            .transition().duration(SYNOPTIC_CONFIG.transitionDuration)
            .attr("opacity", 1);
    };

    private _deleteStationNames = (deletedStations) => {
        deletedStations.select('text')
            .transition().duration(SYNOPTIC_CONFIG.transitionDuration)
            .attr("opacity", 0)
            .remove();
    };

    private _setStationNames = (stationNames) => {
        stationNames
            .attr('x', this._getStationNameRelativeX)
            .attr('y', this._getStationNameRelativeY)
            .attr('transform', this._getStationNameRelativeTransform)
            .attr('fill', SYNOPTIC_CONFIG.stationsColor)
            .attr('opacity', 0)
            .text(([station, lines]) => station.name);
    };

    // STATIONS CONNECTIONS
    private _getCurrentConnexions = (allStations): any => {
        const connexions = allStations.selectAll('g.connection').data(([station, lines]) => lines);
        connexions.attr('transform', (line, i) => `translate(0 ${this._getConnectionY(i)})`);
        this._updateConnectionBg(connexions);
        this._updateConnectionText(connexions);
        return connexions;
    };

    private _addConnections = (currentConnections, onConnectionClick) => {
        const newConnections = currentConnections.enter().append('g')
            .classed('connection', true)
            .attr('transform', (line, i) => `translate(0 ${this._getConnectionY(i)})`);
        this._setConnectionsListeners(newConnections, onConnectionClick);
        this._addConnectionBg(newConnections);
        this._addConnectionsText(newConnections);
    };

    private _deleteConnexions = (currentConnections) => {
        currentConnections.exit().remove();
    };

    private _addConnectionBg = (newConnections) => {
        newConnections.append('circle')
            .attr('r', SYNOPTIC_CONFIG.connectionSize)
            .attr('stroke-width', SYNOPTIC_CONFIG.connectionBorder)
            .attr('stroke', line => line.picto.border)
            .attr('fill', (line) => line.picto.background);
    };

    private _updateConnectionBg = (currentConnections) => {
        currentConnections.select('circle')
            .attr('stroke', line => line.picto.border)
            .attr('fill', (line) => line.picto.background);
    };

    private _addConnectionsText = (newConnections) => {
        const newConnectionTexts = newConnections.append('text')
            .attr('font-size', SYNOPTIC_CONFIG.connectionSize)
            .attr('x', 0 - SYNOPTIC_CONFIG.connectionSize / 4)
            .attr('y', SYNOPTIC_CONFIG.connectionSize / 3);
        this._setConnectionText(newConnectionTexts);
    };

    private _updateConnectionText = (currentConnections) => {
        const currentConnectionTexts = currentConnections.select('text');
        this._setConnectionText(currentConnectionTexts);
    }

    private _setConnectionText = (connectionTexts) => {
        connectionTexts
            .attr('fill', line => line.picto.color)
            .text(line => line.picto.content);
    };

    private _setConnectionsListeners = (newConnections, onConnectionClick) => {
        newConnections
            .style("cursor", "pointer")
            .on('click', function (wp) {
                onConnectionClick(wp);
            });
    };

    // COMMONS CALCULATIONS
    private _getStationX = (index: number): number => {
        return SYNOPTIC_CONFIG.stationRadius + index * (2 * SYNOPTIC_CONFIG.stationRadius + SYNOPTIC_CONFIG.interStation);
    };

    private _getStationY = (): number => {
        return SYNOPTIC_CONFIG.height - SYNOPTIC_CONFIG.stationRadius;
    };

    private _getStationNameRelativeX = (): number => {
        return 1.5 * SYNOPTIC_CONFIG.stationRadius;
    };

    private _getStationNameRelativeY = (): number => {
        return this._getStationY() - SYNOPTIC_CONFIG.stationRadius;
    };

    private _getStationNameRelativeTransform = (): string => {
        const axisX = this._getStationNameRelativeX();
        const axisY = this._getStationNameRelativeY();
        return `rotate(-45 ${axisX} ${axisY})`;
    };

    private _getStationTransform = (i: number): string => {
        const axisX = this._getStationX(i);
        return `translate(${axisX} 0)`;
    };

    private _getConnectionY = (index: number): number => {
        return SYNOPTIC_CONFIG.stationRadius + this._getStationY() + SYNOPTIC_CONFIG.connectionSize * (2.5 * index + 1.5);
    };

    private _getHeightBelow = (stationsWithConnections: StationWithConnections[]) => {
        const connectionCounts = stationsWithConnections.map(([station, connections]) => connections.length);
        const maxConnections = Math.max(...connectionCounts);
        return this._getConnectionY(maxConnections - 1) + SYNOPTIC_CONFIG.connectionSize + SYNOPTIC_CONFIG.connectionBorder - SYNOPTIC_CONFIG.height;
    };
}
