import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LineRoutingModule} from "./line-routing.module";
import {LineComponent} from "./line/line.component";
import {LineListComponent} from "./line-list/line-list.component";
import {MatListModule, MatSidenavModule} from "@angular/material";
import {LineDetailsComponent} from "./line-details/line-details.component";
import {SynopticComponent} from "./synoptic/synoptic.component";
import {SharedModule} from "../shared/shared.module";

@NgModule({
    declarations: [LineComponent, LineListComponent, LineDetailsComponent, SynopticComponent],
    imports: [
        CommonModule,
        LineRoutingModule,
        MatListModule,
        MatSidenavModule,
        SharedModule
    ]
})
export class LineModule {
}
