import {Component, OnInit} from "@angular/core";
import {Observable, zip} from "rxjs";
import {Line} from "../../shared/line/line";
import {LineService} from "../../shared/line/line.service";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {switchMap, tap, map} from "rxjs/internal/operators";
import {StationService} from "../../shared/station/station.service";
import {MapService} from "../../shared/map/map.service";
import {StationWithConnections} from "../synoptic";

@Component({
    selector: 'app-line-details',
    templateUrl: './line-details.component.html',
    styleUrls: ['./line-details.component.less']
})
export class LineDetailsComponent implements OnInit {
    line$: Observable<Line>;
    stationsWithConnections$: Observable<StationWithConnections[]>;

    constructor(private route: ActivatedRoute, private lineService: LineService, private stationService: StationService, private mapService: MapService) {
    }

    ngOnInit() {
        this.line$ = this.route.paramMap.pipe(
            switchMap(this._getLine),
            tap(this._setStations)
        );
    }

    private _getLine = (params: ParamMap): Observable<Line> => {
        return this.lineService.get(+params.get('id'));
    };

    private _setStations = (line: Line) => {
        if (line) {
            const stationsWithConnections = this._getStationsWithConnections(line);

            this.stationsWithConnections$ = zip(...stationsWithConnections).pipe(
                tap(this._toggleLineOnMap)
            );
        }
    };

    private _getStationsWithConnections = (line: Line): Observable<StationWithConnections>[] => {
        const stationIds: number[] = line.stations;
        return stationIds.map(stationId => {
            const station = this.stationService.get(stationId);
            const connections = this._getConnectionsForStation(line, stationId);
            return zip(station, connections);
        });
    };

    private _getConnectionsForStation = (line: Line, stationId: number): Observable<Line[]> => {
        return this.lineService.getConnectionsForStation(stationId).pipe(
            // pop current line from connections list
            map(connections => connections.filter(connection => connection.id !== line.id))
        );
    };

    private _toggleLineOnMap = (stationsWithConnections: StationWithConnections[]) => {
        const stations = stationsWithConnections.map(([station, connections]) => station);
        this.mapService.toggleLine(stations);
    };
}
