import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {LineComponent} from "./line/line.component";
import {LineDetailsComponent} from "./line-details/line-details.component";

const routes: Routes = [
    {
        path: 'lines',
        component: LineComponent,
        children: [
            {
                path: ':id',
                component: LineDetailsComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LineRoutingModule {
}
