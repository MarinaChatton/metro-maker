import {
    Component,
    OnInit,
    Input,
    ElementRef,
    AfterViewInit,
    OnChanges,
    SimpleChanges,
    SimpleChange
} from "@angular/core";
import * as D3Selection from "d3-selection";
import {Router} from "@angular/router";
import {BehaviorSubject} from "rxjs";
import {MapService} from "../../shared/map/map.service";
import {filter} from "rxjs/internal/operators";
import {Synoptic, StationWithConnections} from "../synoptic";
import {Line} from "../../shared/line/line";

@Component({
    selector: 'app-synoptic',
    templateUrl: './synoptic.component.html',
    styleUrls: ['./synoptic.component.less']
})
export class SynopticComponent implements OnInit, AfterViewInit, OnChanges {
    @Input('stationsWithConnections') stationsWithConnections: StationWithConnections[];
    private _onStationsChange: BehaviorSubject<StationWithConnections[]> = new BehaviorSubject([]);
    private _synoptic: Synoptic;

    constructor(private elementRef: ElementRef, private router: Router, private mapService: MapService) {
    }

    ngOnInit() {
        const hostElement = (this.elementRef as any).nativeElement;
        const svg = D3Selection.select(hostElement).select('svg');
        this._synoptic = new Synoptic(svg);
    }

    ngAfterViewInit() {
        // by subscribing on AfterViewInit, allow to wait for the DOM to be rendered before drawing the initial stations set (OnChanges is fired before any other hook on component init)
        this.mapService.onStationFocus.pipe(
            filter(({station, origin}) => origin !== 'synoptic')
        ).subscribe(({station}) => {
            this._synoptic.highlightStation(station);
        });
        this._onStationsChange.subscribe(this._renderSynoptic);
    }

    ngOnChanges(changes: SimpleChanges) {
        const stationsChange2: SimpleChange = changes['stationsWithConnections'];
        if (stationsChange2) {
            this._onStationsChange.next(stationsChange2.currentValue);
        }
    }

    private _renderSynoptic = (stations: StationWithConnections[]) => {
        this._synoptic.render(stations, this._goToStation, this._focusStation, this.mapService.debounceAllStations, this._goToLine);
    };

    private _focusStation = ([station]: StationWithConnections) => {
        this.mapService.bounceStation(station, 'synoptic');
    };

    private _goToStation = ([station]: StationWithConnections) => {
        this.router.navigate(['/stations', station.id]);
    };

    private _goToLine = (line: Line) => {
        this.router.navigate(['/lines', line.id]);
    };
}
