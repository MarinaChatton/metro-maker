import {Component, OnDestroy, ChangeDetectorRef, OnInit} from "@angular/core";
import {MediaMatcher} from "@angular/cdk/layout";

interface NavItem {
    routerLink: string | any[],
    text: string
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {
    title: string = 'metro-maker';
    mobileQuery: MediaQueryList;
    fillerNav: NavItem[] = [];


    private _mobileQueryListener: () => void;

    constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    ngOnInit(): void {
        this.fillerNav = [
            {
                routerLink: ['/stations'],
                text: 'Stations'
            },
            {
                routerLink: ['/lines'],
                text: 'Lines'
            }
        ];
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }
}
