import {Component, OnInit} from "@angular/core";
import {MapService} from "../../shared/map/map.service";
import {Subject} from "rxjs";

@Component({
    selector: 'app-station',
    templateUrl: './station.component.html',
    styleUrls: ['./station.component.less']
})
export class StationComponent implements OnInit {
    displayedStations: number[];
    search: string;
    resize: Subject<boolean> = new Subject();

    constructor(private mapService: MapService) {
    }

    ngOnInit() {
        this.displayedStations = this.mapService.displayedStations;
    }

    reset() {
        this.search = '';
    }

    onDetailsDeactivate() {
        this.resize.next(true);
    }
}
