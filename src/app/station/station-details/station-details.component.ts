import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Observable} from "rxjs";
import {Station} from "../../shared/station/station";
import {switchMap, tap} from "rxjs/internal/operators";
import {StationService} from "../../shared/station/station.service";
import {LineService} from "../../shared/line/line.service";
import {Line} from "../../shared/line/line";
import {MapService} from "../../shared/map/map.service";

@Component({
    selector: 'app-station-details',
    templateUrl: './station-details.component.html',
    styleUrls: ['./station-details.component.less']
})
export class StationDetailsComponent implements OnInit {
    station$: Observable<Station>;
    connections$: Observable<Line[]>;
    displayedStationsOnMap: number[];

    constructor(private route: ActivatedRoute, private stationService: StationService, private lineService: LineService, private router: Router, private mapService: MapService) {
    }

    ngOnInit() {
        this.displayedStationsOnMap = this.mapService.displayedStations;
        this.station$ = this.route.paramMap.pipe(
            switchMap((params: ParamMap) => this.stationService.get(params.get('id'))),
            tap((station: Station) => {
                if (station) {
                    this.connections$ = this.lineService.getConnectionsForStation(station);
                }
            })
        );
    }

    goToLine(line: Line) {
        this.router.navigate(['/lines', line.id]);
    }

    showOnMap(station) {
        this.mapService.toggleStation(station);
        this.mapService.bounceStation(station, 'station-details');
    }

    close() {
        this.router.navigate(['../'], {relativeTo: this.route});
    }

}
