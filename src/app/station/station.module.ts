import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {StationRoutingModule} from "./station-routing.module";
import {StationListComponent} from "./station-list/station-list.component";
import {
    MatListModule,
    MatIconModule,
    MatSidenavModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule
} from "@angular/material";
import {HttpClientModule} from "@angular/common/http";
import {StationDetailsComponent} from "./station-details/station-details.component";
import {StationComponent} from "./station/station.component";
import {LineModule} from "../line/line.module";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {SortStationsByNamePipe} from "../shared/station/sort-stations-by-name.pipe";

@NgModule({
    declarations: [StationListComponent, StationDetailsComponent, StationComponent],
    imports: [
        CommonModule,
        FormsModule,
        StationRoutingModule,
        MatListModule,
        MatIconModule,
        MatSidenavModule,
        MatInputModule,
        MatFormFieldModule,
        MatButtonModule,
        HttpClientModule,
        LineModule,
        SharedModule,
        ScrollingModule
    ],
    providers: [SortStationsByNamePipe]
})
export class StationModule {
}
