import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {StationDetailsComponent} from "./station-details/station-details.component";
import {StationComponent} from "./station/station.component";

const routes: Routes = [
    {
        path: 'stations',
        component: StationComponent,
        children: [
            {
                path: ':id',
                component: StationDetailsComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StationRoutingModule {
}
