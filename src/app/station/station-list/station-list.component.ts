import {Component, OnInit, Input, ViewChild, AfterViewInit} from "@angular/core";
import {Station} from "../../shared/station/station";
import {StationService} from "../../shared/station/station.service";
import {Observable, BehaviorSubject, Subject} from "rxjs";
import {Router, ActivatedRoute} from "@angular/router";
import {MapService} from "../../shared/map/map.service";
import {CdkVirtualScrollViewport} from "@angular/cdk/scrolling";
import {delay, take} from "rxjs/internal/operators";
import {findIndex} from "lodash-es";
import {SortStationsByNamePipe} from "../../shared/station/sort-stations-by-name.pipe";

@Component({
    selector: 'app-station-list',
    templateUrl: './station-list.component.html',
    styleUrls: ['./station-list.component.less']
})
export class StationListComponent implements OnInit, AfterViewInit {
    stations$: Observable<Station[]>;
    selectedId: number;
    displayedStationsOnMap: number[];
    @Input('search') search: string;
    @Input('resize') resize: Subject<boolean>;
    @ViewChild(CdkVirtualScrollViewport) scrollable: CdkVirtualScrollViewport;

    constructor(private stationService: StationService, private router: Router, private route: ActivatedRoute, private mapService: MapService, private sortByStationName: SortStationsByNamePipe) {
    }

    ngOnInit() {
        //check for child route & :id parameter on page load
        const detailsRoute = this.route.firstChild;
        if (detailsRoute) {
            const params = (detailsRoute.params as BehaviorSubject<any>).value;
            this.selectedId = +params.id;
        }

        this.displayedStationsOnMap = this.mapService.displayedStations;
        this.stations$ = this.stationService.getAll();
    }

    ngAfterViewInit() {
        this._scrollOnLoad();// will auto scroll the list if a station is selected on page load
        this._resizeScrollable();// resize the scrollable area to handle viewport height changes on details outlet closing events
    }

    showDetails(station: Station) {
        this.selectedId = station.id;
        this.router.navigate([station.id], {relativeTo: this.route});
    }

    selectedStation(station: Station) {
        this.mapService.toggleStation(station);
    }

    private _scrollOnLoad() {
        this.stations$.pipe(
            delay(100),
            take(1)
        ).subscribe(this._scrollToStation);
    }

    private _resizeScrollable() {
        this.resize.pipe(
            delay(100)
        ).subscribe(() => this.scrollable.checkViewportSize());
    };

    private _scrollToStation = (stations: Station[]) => {
        if (!isNaN(this.selectedId)) {
            const sortedStations = this.sortByStationName.transform(stations);// todo: find a better way to get the stations as loaded in view ?
            const elIndex = findIndex(sortedStations, ['id', this.selectedId]);
            this.scrollable.scrollToIndex(elIndex);
        }
    };
}
