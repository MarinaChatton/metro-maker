import {ILine} from "./line";

export const LINES: ILine[] = [
    {
        id: 1,
        name: 'Line 1',
        picto: {
            background: '#e0e001',
            border: '#e0e001',
            color: 'black',
            content: '1'
        },
        stations: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]
    },
    {
        id: 2,
        name: 'Line 2',
        picto: {
            background: '#004eff',
            border: '#004eff',
            color: 'white',
            content: '2'
        },
        stations: [26, 27, 7, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 21]
    },
    {
        id: 3,
        name: 'Line test',
        picto: {
            background: 'white',
            border: 'red',
            color: 'red',
            content: 'T'
        },
        stations: [1, 4, 5, 29, 21]
    }
];
