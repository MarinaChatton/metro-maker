import {Component, OnInit, Input} from "@angular/core";
import {IPicto} from "../../line/line";

@Component({
    selector: 'app-line-picto',
    templateUrl: 'line-picto.component.html',
    styleUrls: ['line-picto.component.less']
})
export class LinePictoComponent implements OnInit {
    @Input('picto') picto: IPicto;

    constructor() {
    }

    ngOnInit() {
    }

}
