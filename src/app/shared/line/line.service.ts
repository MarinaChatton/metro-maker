import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {ILine, Line} from "./line";
import {map} from "rxjs/internal/operators";
import {LINES} from "./mock-lines";
import {Station} from "../station/station";
import {isNumber} from "util";

@Injectable({
    providedIn: 'root'
})
export class LineService {

    constructor() {
    }

    getAll = (): Observable<ILine[]> => {
        return of(LINES)
            .pipe(
                map(this._hydrateLines)
            );
    };

    get = (id: number | string): Observable<Line> => {
        return this.getAll()
            .pipe(
                map(this._filterLinesById.bind(null, id))
            );
    };

    getConnectionsForStation = (station: Station | number): Observable<Line[]> => {
        const stationId = isNumber(station) ? station : station['id'];
        return this.getAll()
            .pipe(
                map(this._filterLinesByStationId.bind(null, stationId))
            );
    };

    private _hydrateLine = (item: ILine): Line => new Line(item.id, item.name, item.picto, item.stations);

    private _hydrateLines = (items: ILine[]): Line[] => items.map(this._hydrateLine);

    private _filterLineById = (id: number, line: Line): boolean => line.id === +id;

    private _filterLinesById = (id: number, lines: Line[]): Line => (lines as any).find(this._filterLineById.bind(null, id));

    private _filterLineByStationId = (stationId: number, line: Line): boolean => line.stations.indexOf(stationId) > -1;

    private _filterLinesByStationId = (stationId: number, lines: Line[]): Line[] => (lines as any).filter(this._filterLineByStationId.bind(null, stationId));
}
