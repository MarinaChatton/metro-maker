export interface IPicto {
    background: string;
    color: string,
    border: string,
    content: string
}

export interface ILine {
    id: number,
    name: string,
    picto: IPicto,
    stations: number[]
}

export class Line implements ILine {
    id: number;
    name: string;
    picto: IPicto;
    stations: number[];

    constructor(id: number, name: string, picto: IPicto, stations: number[]) {
        this.id = id;
        this.name = name;
        this.picto = picto;
        this.stations = stations;
    }
}
