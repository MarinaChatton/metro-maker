import {LatLngLiteral} from "leaflet";
export interface IStation {
    id: number,
    name: string,
    coords: LatLngLiteral
}

export class Station implements IStation {
    id: number;
    name: string;
    coords: LatLngLiteral;

    constructor(id: number, name: string, coords: LatLngLiteral) {
        this.id = id;
        this.name = name;
        this.coords = coords;
    }
}
