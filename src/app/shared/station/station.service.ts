import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import {IStation, Station} from "./station";
import {STATIONS} from "./mock-stations";

@Injectable({
    providedIn: 'root'
})
export class StationService {

    constructor() {
    }

    getAll = (): Observable<IStation[]> => {
        return of(STATIONS)
            .pipe(
                map(this._hydrateStations)
            );
    };

    get = (id: number | string): Observable<Station> => {
        return this.getAll()
            .pipe(
                map(this._filterStationsById.bind(null, id))
            );
    };

    private _hydrateStation = (item: IStation): Station => new Station(item.id, item.name, item.coords);

    private _hydrateStations = (items: IStation[]): Station[] => items.map(this._hydrateStation);

    private _filterStationById = (id: number, station: Station): boolean => station.id === +id;

    private _filterStationsById = (id: number, stations: Station[]): Station => (stations as any).find(this._filterStationById.bind(null, id));
}
