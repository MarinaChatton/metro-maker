import {Pipe, PipeTransform} from "@angular/core";
import {Station} from "./station";
import {orderBy} from "lodash-es";

@Pipe({
    name: 'sortStationsByName'
})
export class SortStationsByNamePipe implements PipeTransform {

    transform(stations: Station[]): Station[] {
        return orderBy(stations, ['name'], ['asc']);
    }

}
