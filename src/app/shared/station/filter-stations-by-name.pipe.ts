import {Pipe, PipeTransform} from "@angular/core";
import {Station} from "./station";
import {filter, trim} from "lodash-es";

@Pipe({
    name: 'filterStationsByName'
})
export class FilterStationsByNamePipe implements PipeTransform {
    constructor() {
    }

    transform(stations: Station[], searchText: string): Station[] {
        const stringToMatch = this._formatString(searchText);
        return stringToMatch.length ? filter(stations, this._isStationNameMatch.bind(null, stringToMatch)) : stations;
    }

    //return a lowercased non-diacritic string stripped from heading/trailing whitespaces
    private _formatString = (stringToFormat: string): string => {
        const trimmedString = trim(stringToFormat);
        const lowercasedString = trimmedString.toLowerCase();
        return lowercasedString.normalize('NFD').replace(/[\u0300-\u036f]/g, "");// replace the diacritics chars with their non-diacritic counterpart
    };

    private _isStationNameMatch = (stringToMatch: string, station: Station): boolean => {
        const stationName = this._formatString(station.name);
        return stationName.indexOf(stringToMatch) > -1;
    }
}
