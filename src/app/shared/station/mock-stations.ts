import {IStation} from "./station";

export const STATIONS: IStation[] = [
    {
        id: 1,
        name: 'La Défense',
        coords: {
            lat: 48.891826754836515,
            lng: 2.237992043215619
        }
    },
    {
        id: 2,
        name: 'Esplanade de la Défense',
        coords: {
            lat: 48.88835806016618,
            lng: 2.249937212862802
        }
    },
    {
        id: 3,
        name: 'Pont de Neuilly',
        coords: {
            lat: 48.88550610941816,
            lng: 2.258527470197358
        }
    },
    {
        id: 4,
        name: 'Les Sablons',
        coords: {
            lat: 48.88129918549113,
            lng: 2.2719151760177914
        }
    },
    {
        id: 5,
        name: 'Porte Maillot',
        coords: {
            lat: 48.87800617627899,
            lng: 2.2824656400013326
        }
    },
    {
        id: 6,
        name: 'Argentine',
        coords: {
            lat: 48.87567248598795,
            lng: 2.289444164481448
        }
    },
    {
        id: 7,
        name: 'Charles de Gaulle - Etoile',
        coords: {
            lat: 48.8739310942679,
            lng: 2.295127251650101
        }
    },
    {
        id: 8,
        name: 'Georges V',
        coords: {
            lat: 48.87204561942636,
            lng: 2.3007691856449535
        }
    },
    {
        id: 9,
        name: 'Franklin D. Roosevelt',
        coords: {
            lat: 48.869010427937624,
            lng: 2.3102531808960265
        }
    },
    {
        id: 10,
        name: 'Champs Elysées - Clemenceau',
        coords: {
            lat: 48.867744026068706,
            lng: 2.3141227803741415
        }
    },
    {
        id: 11,
        name: 'Concorde',
        coords: {
            lat: 48.86567809641779,
            lng: 2.321193771801867
        }
    },
    {
        id: 12,
        name: 'Tuileries',
        coords: {
            lat: 48.86478016250008,
            lng: 2.3290949544294275
        }
    },
    {
        id: 13,
        name: 'Palais Royal - Musée du Louvre',
        coords: {
            lat: 48.8623718215243,
            lng: 2.3365736007364304
        }
    },
    {
        id: 14,
        name: 'Louvres - Rivoli',
        coords: {
            lat: 48.86087985759375,
            lng: 2.340973275817903
        }
    },
    {
        id: 15,
        name: 'Châtelet',
        coords: {
            lat: 48.85856967247972,
            lng: 2.347933245835307
        }
    },
    {
        id: 16,
        name: 'Hôtel de Ville',
        coords: {
            lat: 48.857355927169515,
            lng: 2.3520735668587185
        }
    },
    {
        id: 17,
        name: 'Saint-Paul',
        coords: {
            lat: 48.85513451823504,
            lng: 2.3613343390676635
        }
    },
    {
        id: 18,
        name: 'Bastille',
        coords: {
            lat: 48.85297567465423,
            lng: 2.3692188244433434
        }
    },
    {
        id: 19,
        name: 'Gare de Lyon',
        coords: {
            lat: 48.84555978708341,
            lng: 2.37344920496318
        }
    },
    {
        id: 20,
        name: 'Reuilly-Diderot',
        coords: {
            lat: 48.8473528769588,
            lng: 2.3858425400402448
        }
    },
    {
        id: 21,
        name: 'Nation',
        coords: {
            lat: 48.84811123157566,
            lng: 2.3980040127977436
        }
    },
    {
        id: 22,
        name: 'Porte de Vincennes',
        coords: {
            lat: 48.847016518419125,
            lng: 2.410816882334764
        }
    },
    {
        id: 23,
        name: 'Saint-Mandé',
        coords: {
            lat: 48.84623825662077,
            lng: 2.418999828109256
        }
    },
    {
        id: 24,
        name: 'Bérault',
        coords: {
            lat: 48.84536875015732,
            lng: 2.428244509620486
        }
    },
    {
        id: 25,
        name: 'Château de Vincennes',
        coords: {
            lat: 48.84432514436857,
            lng: 2.440552346211822
        }
    },
    {
        id: 26,
        name: 'Porte Dauphine',
        coords: {
            lat: 48.87179221641751,
            lng: 2.27486996119569
        }
    },
    {
        id: 27,
        name: 'Victor Hugo',
        coords: {
            lat: 48.869864912484076,
            lng: 2.285226207250439
        }
    },
    {
        id: 28,
        name: 'Ternes',
        coords: {
            lat: 48.87822804050767,
            lng: 2.2981214325821844
        }
    },
    {
        id: 29,
        name: 'Courcelles',
        coords: {
            lat: 48.87927215163157,
            lng: 2.303298546682486
        }
    },
    {
        id: 30,
        name: 'Monceau',
        coords: {
            lat: 48.88057689074842,
            lng: 2.309415946164807
        }
    },
    {
        id: 31,
        name: 'Villiers',
        coords: {
            lat: 48.88132429394468,
            lng: 2.3165967615088174
        }
    },
    {
        id: 32,
        name: 'Rome',
        coords: {
            lat: 48.88207083393704,
            lng: 2.3203983393099863
        }
    },
    {
        id: 33,
        name: 'Place de Clichy',
        coords: {
            lat: 48.88345565159549,
            lng: 2.3273749934103556
        }
    },
    {
        id: 34,
        name: 'Blanche',
        coords: {
            lat: 48.88377051883179,
            lng: 2.332485148274094
        }
    },
    {
        id: 35,
        name: 'Pigalle',
        coords: {
            lat: 48.88242252841757,
            lng: 2.337254682101535
        }
    },
    {
        id: 36,
        name: 'Anvers',
        coords: {
            lat: 48.882871690180586,
            lng: 2.344163572800391
        }
    },
    {
        id: 37,
        name: 'Barbès - Rochechouart',
        coords: {
            lat: 48.88368010927906,
            lng: 2.3495328036519307
        }
    },
    {
        id: 38,
        name: 'La Chapelle',
        coords: {
            lat: 48.884388698358165,
            lng: 2.3592765642767297
        }
    },
    {
        id: 39,
        name: 'Stalingrad',
        coords: {
            lat: 48.88418929420877,
            lng: 2.367030441974567
        }
    },
    {
        id: 40,
        name: 'Jaurès',
        coords: {
            lat: 48.88276854674882,
            lng: 2.36994579899074
        }
    },
    {
        id: 41,
        name: 'Colonel Fabien',
        coords: {
            lat: 48.87818498179219,
            lng: 2.3701745466363584
        }
    },
    {
        id: 42,
        name: 'Belleville',
        coords: {
            lat: 48.87228732058717,
            lng: 2.3767376773224687
        }
    },
    {
        id: 43,
        name: 'Couronnes',
        coords: {
            lat: 48.86932916949657,
            lng: 2.3805363661617247
        }
    },
    {
        id: 44,
        name: 'Ménilmontant',
        coords: {
            lat: 48.865705787623,
            lng: 2.3844293751632057
        }
    },
    {
        id: 45,
        name: 'Père Lachaise',
        coords: {
            lat: 48.86316120506396,
            lng: 2.3872604001843536
        }
    },
    {
        id: 46,
        name: 'Philippe auguste',
        coords: {
            lat: 48.85837884146994,
            lng: 2.3897347327397043
        }
    },
    {
        id: 47,
        name: 'Alexandre Dumas',
        coords: {
            lat: 48.856426348325236,
            lng: 2.394554257654992
        }
    },
    {
        id: 48,
        name: 'Avron',
        coords: {
            lat: 48.851193819213286,
            lng: 2.398239140823123
        }
    }
];
