import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LinePictoComponent} from "./line/line-picto/line-picto.component";
import {MapComponent} from "./map/map/map.component";
import {FilterStationsByNamePipe} from "./station/filter-stations-by-name.pipe";
import {SortStationsByNamePipe} from "./station/sort-stations-by-name.pipe";

@NgModule({
    declarations: [
        LinePictoComponent,
        MapComponent,
        FilterStationsByNamePipe,
        SortStationsByNamePipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        LinePictoComponent,
        MapComponent,
        FilterStationsByNamePipe,
        SortStationsByNamePipe
    ]
})
export class SharedModule {
}
