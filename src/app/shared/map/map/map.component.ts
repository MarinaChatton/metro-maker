import {Component, OnInit, OnDestroy} from "@angular/core";
import * as L from "leaflet";
import {LatLngExpression} from "leaflet";
import {MapService} from "../map.service";
import {Station} from "../../station/station";
import {MAP_CONFIG} from "../map.config";

@Component({
    selector: 'app-map',
    templateUrl: 'map.component.html',
    styleUrls: ['map.component.less']
})
export class MapComponent implements OnInit, OnDestroy {
    map: any;

    constructor(private mapService: MapService) {
    }

    ngOnInit() {
        this._initMap();

        this.mapService.onStationsChange.subscribe(this._fitAllStations);

        this.mapService.onStationFocus.subscribe(this._panToFocusedStation);
    }

    private _initMap() {
        this.map = L.map('mapid').setView((MAP_CONFIG.initCoords as LatLngExpression), MAP_CONFIG.initZoom);
        L.tileLayer(MAP_CONFIG.url, MAP_CONFIG.options).addTo(this.map);
        this.mapService.stationsLayer.addTo(this.map);
        this.mapService.lineLayer.addTo(this.map);
    }

    private _fitAllStations = () => {
        const bounds = this.mapService.stationsLayer.getBounds();
        if (bounds.isValid()) {
            this.map.fitBounds(bounds, {maxZoom: MAP_CONFIG.initZoom});
        }
    };

    private _panToFocusedStation = ({station}: {station: Station}) => {
        this.map.panTo(station.coords);
    };

    ngOnDestroy() {
        this.mapService.clearAll();
    }
}
