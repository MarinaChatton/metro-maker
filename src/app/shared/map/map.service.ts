import {Injectable} from "@angular/core";
import {Station} from "../station/station";
import * as L from "leaflet";
import {icon, Marker} from "leaflet";
import {pull} from "lodash-es";
import {Subject} from "rxjs";
import {MAP_CONFIG} from "./map.config";

/*
 fix for wrong url on markers:
 https://github.com/Leaflet/Leaflet/issues/4968#issuecomment-419634375
 */
const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = icon({
    iconRetinaUrl,
    iconUrl,
    shadowUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41]
});

Marker.prototype.options.icon = iconDefault;

@Injectable({
    providedIn: 'root'
})
export class MapService {
    readonly displayedStations: number[] = [];
    readonly stationsLayer: any = L.featureGroup();
    readonly lineLayer: any = L.polyline([], MAP_CONFIG.pathStyle.standard);
    onStationsChange: Subject<boolean> = new Subject();
    onStationFocus: Subject<{station: Station, origin?: string}> = new Subject();

    constructor() {
    }

    toggleLine = (stations: Station[]) => {
        const coords = stations.map(station => station.coords);
        this.lineLayer.setLatLngs(coords);
        this._clearStations();
        stations.forEach(this._addStation);
        this.onStationsChange.next(true);
    };

    toggleStation = (station: Station) => {
        const isNew = !this._getStationLayer(station);
        if (isNew) {
            this._addStation(station);
        } else {
            this._removeStation(station);
        }
        this.onStationsChange.next(true);
    };

    clearAll = () => {
        this.lineLayer.setLatLngs([]);
        this.stationsLayer.clearLayers();
        this.displayedStations.length = 0;
    };

    bounceStation = (station: Station, origin: string) => {
        this.debounceAllStations();
        const circleMarker: any = this._getStationLayer(station);
        if (circleMarker) {
            this._bounceLayer(circleMarker);
            this.onStationFocus.next({station, origin});
        }
    };

    debounceAllStations = () => {
        this.stationsLayer.eachLayer((layer: any) => {
            layer.setStyle(MAP_CONFIG.pathStyle.standard);
            layer.closePopup();
        });
    };

    private _bounceLayer = (layer) => {
        layer.setStyle(MAP_CONFIG.pathStyle.highlight);
        layer.openPopup();
    };

    private _clearStations = () => {
        this.stationsLayer.clearLayers();
        this.displayedStations.length = 0;
    };
    private _addStation = (station: Station) => {
        if (!this._getStationLayer(station)) {
            const newLayer: any = L.circle(station.coords, MAP_CONFIG.pathStyle.standard);
            newLayer.setRadius(MAP_CONFIG.stationRadius);
            const popup = `Station : ${station.name}`;
            newLayer.bindPopup(popup);
            newLayer.on('click', () => {
                this.debounceAllStations();
                this._bounceLayer(newLayer);
                this.onStationFocus.next({station});
            });
            newLayer.stationId = station.id;
            this.stationsLayer.addLayer(newLayer);
            this.displayedStations.push(station.id);
        }
    };

    private _removeStation = (station: Station) => {
        const stationLayer = this._getStationLayer(station);
        if (stationLayer) {
            this.stationsLayer.removeLayer(stationLayer);
            pull(this.displayedStations, station.id);
        }
    };

    private _getStationLayer = (station: Station): Marker => {
        const addedLayers = this.stationsLayer.getLayers();
        return addedLayers.find(layer => layer.stationId === station.id);
    };
}
